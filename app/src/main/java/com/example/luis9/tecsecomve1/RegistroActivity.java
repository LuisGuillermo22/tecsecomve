package com.example.luis9.tecsecomve1;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText nombre, correo, contraseña, confcontraseña;
    private Button registrarse;
    private ProgressDialog progressDialog;

    //Declaramos un objecto firebaseAuth
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        //inicializamos el objecto firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();

        registrarse =(Button)findViewById(R.id.Registrarse);
        registrarse.setOnClickListener(this);

        nombre = (EditText) findViewById(R.id.txt_Nombre);
        correo = (EditText) findViewById(R.id.txt_Correo);
        contraseña = (EditText) findViewById(R.id.txt_Contraseña);
        confcontraseña = (EditText) findViewById(R.id.Confcontraseña);
        progressDialog = new ProgressDialog(this);



    }

    private void registrarUsuario(){

        //obtenemos el email y la contraseña desde las cajas de texto
        String usuario = nombre.getText().toString().trim();
        String email = correo.getText().toString().trim();
        String password = contraseña.getText().toString().trim();
        String password2 = confcontraseña.getText().toString().trim();

        //Verificamos que las cajas de texto no esten vacias

        if (TextUtils.isEmpty(usuario)){
            Toast.makeText(this, "Se debe ingresar un nombre de Usuario",Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(email)){
            Toast.makeText(this, "Se debe ingresar un email",Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password)){
            Toast.makeText(this, "Falta Ingresar la Contraseña",Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password2)){
            Toast.makeText(this, "Falta Ingresar la Confirmacion de su Contraseña",Toast.LENGTH_LONG).show();
            return;
        }

        progressDialog.setMessage("Realizando el Registro...");
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(email, password ).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){

                    Toast.makeText(RegistroActivity.this,"Se ha registrado el email", Toast.LENGTH_LONG).show();

                }else{

                    Toast.makeText(RegistroActivity.this,"No se pudo Registrar el Usuario", Toast.LENGTH_LONG).show();
                }

                progressDialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {

        //invocamos al metodo:
        registrarUsuario();


    }
}
